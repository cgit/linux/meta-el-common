IMAGE_FEATURES[validitems] += "read-only-rootfs empty-root-password allow-empty-password post-install-logging "

IMAGE_FEATURES += " \
                   dbg-pkgs \
                   debug-tweaks \
                   dev-pkgs \
                   doc-pkgs \
                   splash \
                   staticdev-pkgs \
                   eclipse-debug \
                   hwcodecs \
                   nfs-server \
                   ssh-server-openssh \
                   tools-debug \
                   tools-profile \
                   tools-sdk \
                   tools-testapps \
                   x11 \
                   x11-base"

IMAGE_INSTALL += " \
    packagegroup-base \
    packagegroup-enea-rt-tools \
    ${ROOTFS_PKGMANAGE_BOOTSTRAP} \
    beecrypt \
    binutils \
    chkconfig \
    cracklib \
    curl \
    dhcp-client \
    eventlog \
    file \
    findutils \
    freetype \
    fuse \
    gawk \
    gcc \
    gnutls \
    grep \
    gzip \
    icu \
    kbd \
    kernel-dev \
    kernel-modules \
    libgcrypt \
    libgpg-error \
    libtasn1 \
    lsb \
    lsbinitscripts \
    mingetty \
    openssh \
    ossp-uuid \
    perf \
    pkgconfig \
    procps \
    psmisc \
    rpm \
    tar \
    tipcutils \
    udev-extraconf \
    zip \
    "
