IMAGE_FEATURES += "ssh-server-dropbear package-management"

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    "

EXTRA_IMAGEDEPENDS_remove = " fm-ucode"

IMAGE_LINGUAS = ""

LICENSE = "MIT"

inherit core-image
