MIRRORS_append = "\
cvs://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
svn://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
git://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
hg://.*/.*      http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
bzr://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
p4://.*/.*      http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
osc://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
https?$://.*/.* http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
ftp://.*/.*     http://linux.enea.com/${DISTRO_VERSION}/sources/ \n \
"
