FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "\
        file://add-ptest.patch \
        file://run-ptest \
        file://avoid_parallel_tests.patch \
"

inherit ptest

do_compile_ptest() {
    oe_runmake -C tests buildtest-TESTS
}

do_install_ptest() {
	oe_runmake -C tests install-ptest DESTDIR=${D}${PTEST_PATH}
}

RDEPENDS_${PN}-ptest += " bash"
