inherit ptest

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://run-ptest \
            file://runtest-flags.patch \
            file://0001-add-delay-before-run-exp-file.patch \
            file://0002-function-name-is-used-by-mistake.patch \
            file://gdb-ptest-attach.patch \
           "



DEPENDS_${PN}-ptest += "expect dejagnu tcl"
RDEPENDS_${PN}-ptest += "glibc-charmap-ibm1047 \
                         glibc-gconv-ibm1047 \
                         glibc-charmap-ebcdic-us \
                         glibc-gconv-ebcdic-us \
                         glibc-gconv-utf-32 \
                         glibc-gconv-utf-16 \
                         prelink \
                         expect \
                         dejagnu \
                         make \
                         bash \
                        "
do_configure_ptest () {
    cd ${S}/gdb/testsuite/
    ./configure --host=${HOST_SYS} --build=${BUILD_SYS}
    make site.exp
}

do_install_ptest () {
    cp -pr ${S}/gdb/testsuite/ ${D}${PTEST_PATH}
    cp -pr ${S}/gdb/features/  ${D}${PTEST_PATH}
    install -p  ${S}/config.sub     ${D}${PTEST_PATH}/testsuite/
    install -p  ${S}/config.guess   ${D}${PTEST_PATH}/testsuite/
    install -p  ${S}/install-sh     ${D}${PTEST_PATH}/testsuite/
}
