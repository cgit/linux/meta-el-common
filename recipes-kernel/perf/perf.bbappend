#
# added the python-math and python-sqlite3 modules, required by perf
#
SCRIPTING_RDEPENDS = "${@perf_feature_enabled('perf-scripting', 'perl perl-modules python python-math python-sqlite3', '',d)}"
