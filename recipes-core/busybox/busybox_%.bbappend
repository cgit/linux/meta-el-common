do_prepare_config_append () {
      sed -i -e 's/# CONFIG_CHRT is not set/CONFIG_CHRT=y/' .config
      sed -i -e 's/# CONFIG_TASKSET is not set/CONFIG_TASKSET=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_TASKSET_FANCY is not set/CONFIG_FEATURE_TASKSET_FANCY=y/' .config
      sed -i -e 's/# CONFIG_HTTPD is not set/CONFIG_HTTPD=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_RANGES is not set/CONFIG_FEATURE_HTTPD_RANGES=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_USE_SENDFILE is not set/CONFIG_FEATURE_HTTPD_USE_SENDFILE=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_SETUID is not set/CONFIG_FEATURE_HTTPD_SETUID=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_BASIC_AUTH is not set/CONFIG_FEATURE_HTTPD_BASIC_AUTH=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_AUTH_MD5 is not set/CONFIG_FEATURE_HTTPD_AUTH_MD5=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_CGI is not set/CONFIG_FEATURE_HTTPD_CGI=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_CONFIG_WITH_SCRIPT_INTERPR is not set/CONFIG_FEATURE_HTTPD_CONFIG_WITH_SCRIPT_INTERPR=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_SET_REMOTE_PORT_TO_ENV is not set/CONFIG_FEATURE_HTTPD_SET_REMOTE_PORT_TO_ENV=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_ENCODE_URL_STR is not set/CONFIG_FEATURE_HTTPD_ENCODE_URL_STR=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_ERROR_PAGES is not set/CONFIG_FEATURE_HTTPD_ERROR_PAGES=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_PROXY is not set/CONFIG_FEATURE_HTTPD_PROXY=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_HTTPD_GZIP is not set/CONFIG_FEATURE_HTTPD_GZIP=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_TAR_NOPRESERVE_TIME is not set/CONFIG_FEATURE_TAR_NOPRESERVE_TIME=y/' .config
}
