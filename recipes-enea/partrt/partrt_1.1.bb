SUMMARY = "CPU partitioning tool"
DESCRIPTION = "partrt is a tool for dividing a SMP Linux system into a real time domain and a non-real time domain."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

RDEPENDS_${PN} = "bash"
RDEPENDS_${PN}-ptest += "python"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://run-ptest \
           "

SRCREV = "cbe36a4946a2b3bb4927ca3b8ac800111ae9ce49"

inherit ptest

S = "${WORKDIR}/git"

FILES_${PN} += "/bin/*"

do_install() {
        install -d ${D}/usr/bin
        install ${S}/install/bin/partrt ${D}/usr/bin
}

do_install_ptest() {
        install ${S}/test/test_partition.py ${D}${PTEST_PATH}
        sed -i s/target/${MACHINE}/ ${D}${PTEST_PATH}/run-ptest
}
