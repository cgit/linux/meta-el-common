SUMMARY = "LINX for Linux fast IPC"
DESCRIPTION = "LINX is a distributed communication protocol stack for transparent inter node and interprocess communication for a heterogeneous mix of systems."
HOMEPAGE = "http://linx.sourceforge.net/"

SECTION = "kernel/modules"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../../COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

DEPENDS = "linux-libc-headers linx"
RRECOMMENDS_${PN} = "linx kmod"

SRC_URI = "http://linux.enea.com/linx/linx-${PV}.tar.gz"

SRC_URI[md5sum] = "f9d7634faa2d7338e51418e2dca82875"
SRC_URI[sha256sum] = "d047eb8d4b63ae385bf89dc3cc09854a0fe27f3f51c19dc332da7d40f9c0c28c"

S = "${WORKDIR}/linx-${PV}/net/linx"

inherit module

module_do_compile_prepend () {

        if [ -z $(cat ${STAGING_KERNEL_BUILDDIR}/.config | grep "CONFIG_MODULES=y") ] ; then
            echo "The specified Linux kernel has no support for pluggable modules (CONFIG_MODULES=y). Enable it before baking this."
            exit 1
        fi

        do_make_scripts
}
