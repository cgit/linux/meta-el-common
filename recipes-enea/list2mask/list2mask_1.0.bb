SUMMARY = "Translate CPU list to a CPU mask"
DESCRIPTION = "Translate CPU list given on command line to a hexadecimal CPU mask. Can use kernel boot parameters isolcpus or nohz_full as input as well as a list given on command line."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

PR = "r1"

RDEPENDS_${PN} = "bash"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://run-ptest \
           "

SRCREV = "db6eff03d80c04803fb146939c504b500e16fe2f"

inherit ptest

S = "${WORKDIR}/git"

FILES_${PN} += "/bin/*"

do_install() {
        install -D ${S}/install/bin/${PN} ${D}/usr/bin/${PN}
}
