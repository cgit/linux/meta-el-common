SUMMARY = "Tick count tool"
DESCRIPTION = "Count number of kernel ticks during command execution."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

PR = "r1"

RDEPENDS_${PN} = "bash"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://run-ptest \
           "

SRCREV = "0fa0a8e084fe68e77a1f0968f2fbfa993292ae9c"

inherit ptest

S = "${WORKDIR}/git"

FILES_${PN} += "/bin/*"

do_install() {
        install -D ${S}/install/bin/count_ticks ${D}/usr/bin/count_ticks
}
