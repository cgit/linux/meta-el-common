FILESEXTRAPATHS_prepend := "${THISDIR}/connman:"

SRC_URI += "file://0001-added-noipconfig-option.patch \
            file://connman.patch \
           "

python do_patch_prepend() {
    import shutil
    workdir = d.getVar('WORKDIR', True)
    source = d.getVar('S', True)
    shutil.move(os.path.join(workdir, "connman"), source)
}


python do_patch_append() {
    import shutil
    workdir = d.getVar('WORKDIR', True)
    source = d.getVar('S', True)
    shutil.move(os.path.join(source, "connman"), workdir)
}
